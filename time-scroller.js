// The time scrubber allowing user to move through time
import React, {Component, createElement} from 'react';
import PropTypes from 'prop-types';
import MapGL, {Marker, Popup, BaseControl} from 'react-map-gl';
// import autobind from '../utils/autobind';

const propTypes = Object.assign({}, BaseControl.propTypes, {
  // Custom className
  className: PropTypes.string,
  // Longitude of the anchor point
  min: PropTypes.number.isRequired,
  // Latitude of the anchor point
  max: PropTypes.number.isRequired,
  // Offset from the left
  offsetLeft: PropTypes.number,
  // Offset from the top
  offsetTop: PropTypes.number
});

const defaultProps = Object.assign({}, BaseControl.defaultProps, {
  className: '',
  offsetLeft: 0,
  offsetTop: 0
});

export default class TimeScroller extends BaseControl {

  constructor (props)
  {
    super(props);
    // console.log(props);
    this.state = {
      value: props.value,
      min: props.min,
      max: props.max
    };

    this.handleChange = this.handleChange.bind(this); 
  }

  handleChange(event) 
  {
    // this.setState({value: event.target.value});
    this.props.onTimeScrollerChange(event.target.value);
  } 

  render() {
    // return (
      // <input id = 'time-slider' className = 'time-slider' type='range' min={this.state.min} max={this.state.max} step='10' value={this.state.value} onChange={this.handleChange}  />
      return createElement('input', {
        className: `time-slider`,
        ref: this._onContainerLoad,
        type: `range`,
        min: this.state.min,
        max: this.state.max,
        step: 100, 
        value: this.props.value,
        children: this.props.children,
        onChange : this.handleChange
      });
    // )
  }

}

TimeScroller.displayName = 'TimeScroller';
TimeScroller.propTypes = propTypes;
TimeScroller.defaultProps = defaultProps;
