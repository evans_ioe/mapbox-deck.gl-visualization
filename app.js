/* global window,document */
import React, {Component} from 'react';
import {render} from 'react-dom';
// import MapGL from 'react-map-gl';
import MapGL, {Marker, Popup, NavigationControl, BaseControl} from 'react-map-gl';
import DeckGLOverlay from './deckgl-overlay.js';
import TimeScroller from './time-scroller.js';
import View from './view.js';

import {json as requestJson} from 'd3-request';

// Set your mapbox token here
const MAPBOX_TOKEN = 'pk.eyJ1IjoicmFmZm1hcmVzIiwiYSI6ImNqOGYwdXd6YTE0ZnczMm1uMTF0NzZnbDYifQ.apQdLo_KQAc1jDQIKKTDHQ'; // eslint-disable-line

// Source data CSV
const DATA_URL = {
  TRIPS: 'https://bitbucket.org/raffmares/sc_test_data/raw/1f1c5d134d97836af4f1881ed70b63621447b556/wildlife_test_data.json',  // eslint-disable-line
  ICONS : 'https://raw.githubusercontent.com/uber-common/deck.gl-data/master/examples/icon/meteorites.json' // eslint-disable-line
};

const navStyle = {
  position: 'absolute',
  top: 0,
  right: 0,
  padding: '10px'
};

class Root extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      viewport: {
        ...DeckGLOverlay.defaultViewport,
        width: 500,
        height: 500
      },
      trips: null,
      time: 0,
      active: true,
      data: null,
      timestamp: 0
    };

    // this._timestamp = 0;
    this.loopLength = 21540;
    this.loopTime = 30000;

    this.handleTimeScrollerChange = this.handleTimeScrollerChange.bind(this);

    requestJson(DATA_URL.TRIPS, (error, response) => {
      if (!error) {
        this.setState({trips: response});
      }
    });

    requestJson(DATA_URL.ICONS, (error, response) => {
      if (!error) {
        this.setState({data: response});
      }
    });

    requestJson('./data/location-icon-mapping.json', (error, response) => {
      if (!error) {
        this.setState({iconMapping: response});
      }
    });
  }

  componentDidMount() {
    window.addEventListener('resize', this._resize.bind(this));
    this._resize();
    this._animate();
  }

  componentWillUnmount() {
    if (this._animationFrame) {
      window.cancelAnimationFrame(this._animationFrame);
    }
  }

  _animate() {
    if(null == this._timestamp)
    {
      this._timestamp = 0;
    }

    if(this.state.active) {
      if( (this._timestamp + 20) < this.loopLength )
      {
        // Math.round(price / listprice * 100) / 100
        this._timestamp += (Math.round(this.loopTime / this.loopLength * 100) / 100) * 10;
      // } else 
      // {
        // console.log("Tii much : " + this._timestamp + ' ' + this.loopLength) ;
        // this._timestamp = 0;
      }
    }

    
    this.setState(prevState => ({
      time: prevState.active ? (this._timestamp / this.loopTime) * this.loopLength : prevState.time,
      timestamp: this._timestamp
      // time: prevState.active ? ((this._timestamp % loopTime) / loopTime) * loopLength : prevState.time
    }));

    this._currentTime = this.state.time;
    this._animationFrame = window.requestAnimationFrame(this._animate.bind(this));
  }

  _resize() {
    this._onViewportChange({
      width: window.innerWidth,
      height: window.innerHeight
    });
  }

  handleTimeScrollerChange (value)
  {
    this._timestamp = value;
    this._timestamp = Math.round(this._timestamp * 100) / 100;
    console.log("Timestamp : " + this._timestamp);

  }

  _onViewportChange(viewport) {
    this.setState({
      viewport: {...this.state.viewport, ...viewport}
    });
  }

  _renderPlayButton ()
  {
    return (
      <button className = 'btn-play' onClick={this._togglePlay.bind(this)}>
        {this.state.active ? 'PAUSE' : 'PLAY'}
      </button>
    );
  }

  _renderTimeInput ()
  {
    return (
      <div className = "current-time">
        {this._currentTime} 
      </div>
    );
  }

  _togglePlay ()
  {
    this.setState(prevState => ({
      active: !prevState.active
    }));
  }

  render() {
    const {viewport, trips, data, iconMapping, time, timestamp} = this.state;

    return (
      // <View>
      // <TimeScroller value={this._timestamp} min={0} max={this.loopLength} onTimeScrollerChange = {this.handleTimeScrollerChange} />
        <MapGL
          {...viewport}
          mapStyle="mapbox://styles/mapbox/outdoors-v9"
          onViewportChange={this._onViewportChange.bind(this)}
          mapboxApiAccessToken={MAPBOX_TOKEN}>

          <div className="nav" style={navStyle}>
            <NavigationControl onViewportChange={this._onViewportChange.bind(this)} />
          </div>

          {this._renderPlayButton()} 
          {this._renderTimeInput()}

          <TimeScroller value={this._timestamp} min={0} max={this.loopLength} onTimeScrollerChange = {this.handleTimeScrollerChange} />

          <DeckGLOverlay viewport={viewport}
            // iconAtlas="images/markers/animal.png"
            trips={trips}
            data={data}
            iconAtlas="data/location-icon-atlas.png"
            iconMapping={iconMapping}
            showCluster={false}
            trailLength={1800}
            time={time}
            timestamp={timestamp}
          />

        </MapGL>
      // </View>
    );
  }
}

render(<Root />, document.body.appendChild(document.createElement('div')));
