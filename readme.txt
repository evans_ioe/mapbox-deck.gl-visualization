#useful links:
#https://webpack.js.org/guides/installation/
#https://www.codementor.io/javascript/tutorial/module-bundler-webpack-getting-started-guide
#https://mikewilliamson.wordpress.com/2016/02/24/using-mapbox-gl-and-webpack-together/
#https://medium.com/javascript-training/beginner-s-guide-to-webpack-b1f1a3638460


#before running webpack for the first time...
#useful link:
#https://www.sitepoint.com/beginners-guide-node-package-manager/

#install node.js
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs

#maybe not necessary: change location of where global packages are saved
#create a new directory in your home folder.
cd ~ && mkdir .node_modules_global
npm config set prefix=$HOME/.node_modules_global
#re-install npm in new global folder
npm install npm --global
#add .node_modules_global/bin to our $PATH environment variable, so that we can run global packages from the command line
#requires openning .bashrc (see below) and appending the following line at the end of document: export PATH="$HOME/.node_modules_global/bin:$PATH"
sudo xed ~/.bashrc


###
###


#for each project, install all required packages locally (i.e. in the project folder, i.e. in the folder where the html page is saved)
#this is how to access an example project folder: 
cd /home/raff/Documents/work/ioe/safari_central/mapbox-react

#then run this to install everything in one go
npm init --y
npm install --save webpack webpack-dev-server react@15.x react-dom@15.x deck.gl luma.gl mapbox-gl react-map-gl buble-loader

#check if all packages were installed
npm list --depth=0

#then to run webpage without creating bundle.js (see below) and being able to update script on the fly
npm start


###
###


#how to create a bundle using webpack
#in terminal, access the project folder:
cd /home/raff/Documents/work/ioe/safari_central/webmaps_02

#... and run
node_modules/.bin/webpack ./app.js ./bundle.js

